ALTER TABLE `authorities`
    ADD CONSTRAINT `fk_authorities`
        FOREIGN KEY (`username`) REFERENCES `users` (`username`);

insert into users (id, username,password, enabled) values (1, 'admin', '$2a$10$HGohNIHAitEPdw.bZ52nFeuRRsqzWRN1f8MxpAXffTKuy5.gAkGYu', true);
insert into authorities (username, role) values ('admin', 'ROLE_ADMIN' );
insert into users (id, username,password, enabled) values (2, 'user2', '$2a$10$uFS8JeLO8yr6c9zSdZy.G.LiaGANecYXaaWj/E9FPTpDTMkh4ObjC', true);
insert into authorities (username, role) values ('user2', 'ROLE_USER' );
insert into users (id, username,password, enabled) values (3, 'user3', '$2a$10$9YLtXbWqnipz9zfjAAzYRu8I/2GV9FriD./mQzsDSxLZUjMWv3W0O', true);
insert into authorities (username, role) values ('user3', 'ROLE_USER' );

insert into audit_entity(id, bill_generated, creation_date,description, ordered_items, city) values ( 1,500.0, CURRENT_TIMESTAMP,'desc1','[1,2]', 'Delhi' );
insert into audit_entity(id, bill_generated, creation_date,description, ordered_items, city) values ( 2,200.0, CURRENT_TIMESTAMP,'desc2','[1]' , 'Mumbai');
insert into audit_entity(id, bill_generated, creation_date,description, ordered_items, city) values ( 3,200.0, CURRENT_TIMESTAMP,'desc2','[1]' , 'Delhi');
insert into audit_entity(id, bill_generated, creation_date,description, ordered_items, city) values ( 4,200.0, CURRENT_TIMESTAMP,'desc2','[1]' , 'Chennai');
insert into audit_entity(id, bill_generated, creation_date,description, ordered_items, city) values ( 5,200.0, CURRENT_TIMESTAMP,'desc2','[1]' , 'Chennai');

/* view non unique indexed view to see all orders in different cities */
CREATE INDEX SALES_SORTED_BY_CITY ON AUDIT_ENTITY (city);

CHECKPOINT;
/* max sale by price for each month */
CREATE VIEW MAX_SALE_BY_MONTH
AS
SELECT YEAR(creation_date) as SalesYear,
       MONTH(creation_date) as SalesMonth,
       MAX(bill_generated) AS MaxSale
FROM audit_entity
GROUP BY YEAR(creation_date), MONTH(creation_date)
ORDER BY YEAR(creation_date), MONTH(creation_date);

CHECKPOINT;
/* monthly sale total. */
CREATE VIEW MONTHLY_SALE_TOTAL
AS
SELECT YEAR(creation_date) as SalesYear,
       MONTH(creation_date) as SalesMonth,
       SUM(bill_generated) AS TotalSales
FROM audit_entity
GROUP BY YEAR(creation_date), MONTH(creation_date)
ORDER BY YEAR(creation_date), MONTH(creation_date);


