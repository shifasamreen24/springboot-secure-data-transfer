package com.surabi.festivesaleservice.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;

@Slf4j
@RestControllerAdvice
public class RestExceptionHandler {

    @ExceptionHandler(RestaurantCustomException.class)
    public ResponseEntity<RestaurantCustomException> genericException(RestaurantCustomException ex) {
        log.error(ex.getLocalizedMessage());
       final RestaurantCustomException error = new RestaurantCustomException(
                ex.getCode(),
                ex.getMessage(),
                new Date());
       return new ResponseEntity<>(error, error.getCode());
    }

    @ExceptionHandler(value = {NullPointerException.class})
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public RestaurantCustomException resourceNotFoundException(Exception ex, WebRequest request) {
        RestaurantCustomException message = new RestaurantCustomException(
                HttpStatus.NOT_FOUND,
                ex.getMessage(),
                new Date());

        return message;
    }
}
