package com.surabi.festivesaleservice.controller;


import com.surabi.festivesaleservice.entity.CouponEntity;
import com.surabi.festivesaleservice.entity.OrderEntity;
import com.surabi.festivesaleservice.exception.RestaurantCustomException;
import com.surabi.festivesaleservice.repository.CouponRepository;
import com.surabi.festivesaleservice.repository.OrderRepository;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;


@RestController
@RequestMapping("/api/v1")
public class DiscountController {

    @Autowired
    OrderRepository orderRepository;
    @Autowired
    CouponRepository couponRepository;

    //update
    @PatchMapping("/apply_discount_coupon")
    @ApiOperation(value= "Apply coupon to get 50% discount", notes = "Please provide valid order id and coupon code")
    public void applyFestiveDiscountCoupon(@RequestParam String coupon, @RequestParam String orderId) throws RestaurantCustomException {
        Optional<OrderEntity> currentOrder =  orderRepository.findById(orderId);
        Optional<CouponEntity> currentCoupon = couponRepository.findById(coupon);
        if(currentOrder.isPresent() && currentCoupon.isPresent()) {
            try {
                OrderEntity order = currentOrder.get();
                //50% discount
                order.setBill(order.getBill()/2);
                orderRepository.save(order);
            }
            catch (Exception ex){
                throw new RestaurantCustomException("Bill amount updated for given order", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        else {
            throw new RestaurantCustomException("Either order does not exist or coupon is invalid ", HttpStatus.NOT_FOUND);
        }
    }

}
