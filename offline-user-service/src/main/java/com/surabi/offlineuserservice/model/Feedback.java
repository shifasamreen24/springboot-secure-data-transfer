package com.surabi.offlineuserservice.model;

import lombok.*;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Feedback {
    @NotNull
    @Min(0)
    @Max(10)
    private Integer rating;
    private String comments;
}
