package com.surabi.offlineuserservice.service.impl;


import com.surabi.offlineuserservice.entity.MenuEntity;
import com.surabi.offlineuserservice.repository.MenuRepository;
import com.surabi.offlineuserservice.service.IMenuService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class MenuServiceImpl implements IMenuService {

    @Autowired
    MenuRepository menuRepository;

    @Override
    public List<MenuEntity> getAllMenuItems() {
        return menuRepository.findAll();
    }


}
