package com.surabi.offlineuserservice.repository;

import com.surabi.offlineuserservice.entity.EventEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.Date;
import java.util.Optional;

public interface EventRepository extends JpaRepository<EventEntity, Long> {
//    @Query( value = "SELECT * FROM EVENT_ENTITY m WHERE m.date = :inputDate",  nativeQuery = true)
//    EventEntity getEventByDate(@Param("inputDate") Date inputDate);
    Optional<EventEntity> findByDate(LocalDate inputDate);
}
